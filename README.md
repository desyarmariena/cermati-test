# Cermati Front End test

## Desy Rachmawati Armariena

This project is for **Cermati Front end test** using html, plain css and
javascript.

**Deployed URL:
[Netlify](https://master--astounding-caramel-e13876.netlify.app/)**

### Previews

![Mobile View](./assets/mobile-view.png)

![Desktop View](./assets/desktop-view.png)
